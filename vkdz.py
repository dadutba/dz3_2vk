#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Домашнее задание к лекции 3.2 «Работа с классами на примере API VK»"""

#from pprint import pprint
import requests
from time import sleep
#from urllib.parse import urlencode

TOKEN = '195e816eefb57b097c800b8116f9fa9c6a6970f57a7e39524ef4ad16828fb7af38330d8aaca35f92b3dbc'
MAX_REPEAT_REQUESTS = 10
REQUEST_DELAY = 0.3

class VKUser:
    def __init__(self, id):
        for i in range(MAX_REPEAT_REQUESTS):
            response = requests.get(
                'https://api.vk.com/method/users.get',
                params=dict(
                        access_token=TOKEN,
                        v=5.80,
                        user_ids=id
                        )
                )
            if not 'error' in response.json(): break
            sleep(REQUEST_DELAY)
        if 'error' in response.json():
            raise requests.RequestException(
                    response.json()['response']['error_msg'])
        self.id = response.json()['response'][0]['id']
        self.first_name = response.json()['response'][0]['first_name']
        self.last_name = response.json()['response'][0]['last_name']
        
    
    def get_mutual_friends(self, user):
        response = requests.get(
            'https://api.vk.com/method/friends.getMutual',
            params=dict(
                    access_token=TOKEN,
                    v=5.80,
                    source_uid=self.id,
                    target_uid=user.id
                    )
            )
        #return(response.json()['response'])
        return [VKUser(friend) for friend in response.json()['response']]
        
    
    def __repr__(self):
        return f"'https://vk.com/id{self.id}'"
    
    
    def __str__(self):
        return f'https://vk.com/id{self.id}'

    
    def __and__(self, user):
        return self.get_mutual_friends(user)
    
    

def main():    
    romik = VKUser('romikforest')
    olga = VKUser('yazikionline')
    
    print("It's me:", romik)
    
    user_list = romik & olga
    
    print(user_list)
    
    for user in user_list:
        print(user)


if __name__ == '__main__':
    main()

